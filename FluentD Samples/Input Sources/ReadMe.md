
### Setup for demo
Make sure docker is installed. If you want to see only the logs for this demo then remove all containers. 

1. Run fluentD
$ docker-compose up -d

2. Test the inputs from examples found in the folder \test_sources\

3. Check the logs
give it a minute... then look in .\output_logs\my_logs to find the data

4. Turn off fluentD
$ docker-compose down
