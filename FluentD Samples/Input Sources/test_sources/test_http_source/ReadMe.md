You need to do a HTTP request, which looks like the following:

POST http://localhost:12000/all
{"body": "some json"}

## how was this end point configred to work
we told the fluentd to listen for logs on port 12000  
we are expose the port 12000 on the fluentd container
/all tags the log for the match pattern

## how
1. You can run the test-http-source.ps1 or test-http-source.sh
2. You can use postman or a similar tool to do the request
3. your own application
3. Any other way you could post an http request

